import { useState } from 'react';
import { connect } from 'react-redux';
import './LoginCard.scss';
import { submitLoginHandler } from '../../store/Login/actions';
import { Link } from "react-router-dom";

const LoginCard = (props) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmitHandler = (e) => {
    e.preventDefault();
    props.submitHandler(email, password);
  }

  return (
    <div className="LoginCard">
      <div>Log In</div>
      <form onSubmit={onSubmitHandler}>
        <input type="text" placeholder="email" onChange={(e) => setEmail(e.target.value)} />
        <input type="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
        <input type="submit" value="Submit" />
        <Link to="/signup" className="sign-up">Sign Up</Link>
      </form>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    submitHandler: (email, password) => dispatch(submitLoginHandler(email, password))
  }
}

export default connect(null, mapDispatchToProps)(LoginCard);
