import MovieCard from "../MovieCard/MovieCard";
import './MovieList.scss';

const MovieList = (props) => {
  console.log("movie list" + props.movies);
  return (
    <div className="MovieList">
      {
        props.movies.map(movie => {
          return <MovieCard key={movie.id} movie={movie}/>
        })
      }
    </div>
  );
}

export default MovieList;