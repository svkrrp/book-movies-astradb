import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import './Nav.scss';
import { logoutHandler } from '../../store/Login/actions';

const Nav = (props) => {

  const onCLickHandler = () => {
    if(props.login === 'Login') {
      props.history.push("/");
    } else {
      props.logoutHandler();
    }
  }

  return (
    <div className="Nav">
      <Link className="upload" to="/">Book Movie Tickets</Link>
      <div className="back">
        <Link className="upload" to="/movieupload">Upload</Link>
        <div className="login" onClick={onCLickHandler}>{props.login}</div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    login: state.login.user ? "Logout" : "Login"
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logoutHandler: () => dispatch(logoutHandler())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Nav));