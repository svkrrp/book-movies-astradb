import './Seat.scss';

const Seat = (props) => {
  return (
    <div className={`Seat ${props.selectedSeats.includes(props.seatNumber) ? 'blue' : ''}`} onClick={() => props.setSelectedSeats([...props.selectedSeats, props.seatNumber])}>
      <img src={props.isSelected ? 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1cE_UdzIjjIzv7DAIhHryTP2Zuziu3VRx2A&usqp=CAU' : 'https://library.kissclipart.com/20180906/uaq/kissclipart-car-back-seat-icon-clipart-car-automotive-seats-co-1ed67824a97f9eab.png'} height="40px"/>
      <div className="num">{props.seatNumber}</div>
    </div>
  );
}

export default Seat;