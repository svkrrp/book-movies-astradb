import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './MovieCard.scss';
import { removeMovieHandler } from '../../store/Movie/actions';

const MovieCard = (props) => {

  const { name, imageUrl, time, id } = props.movie;

  const removeMovieHandler = () => {
    props.removeMovie(id);
  }

  return (
    <div className="MovieCard">
      <div className="image">
        <img src={imageUrl} alt="movie image"/>
      </div>
      <div className="details">
        <div className="movie-name">{name}</div>
        <div className="movie-time">{time}</div>
        <Link to={`/hall/${id}`} className="book-now">Book Now</Link>
        <button onClick={removeMovieHandler}>Remove Movie</button>
      </div>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    removeMovie: (id) => dispatch(removeMovieHandler(id))
  }
} 

export default connect(null, mapDispatchToProps)(MovieCard);