import { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Seat from "../Seat/Seat";
import './Seats.scss';
import { getSeatHandler, selectSeatHandler } from '../../store/Seat/action';

const Seats = (props) => {

  const movieId = props.match.params.id;

  const [selectedSeats, setSelectedSeats] = useState([]);

  useEffect(() => {
    props.getSeats(props.match.params.id)
  }, [])

  const onConfirmHandler = () => {
    props.selectSeat(selectedSeats, movieId, props.user);
    setSelectedSeats([]);
  }

  return (
    <>
      <div className="Seats">
        {
          Object.keys(props.seats).map((val) => {
            return <Seat key={val} selectedSeats={selectedSeats} setSelectedSeats={setSelectedSeats} seatNumber={val} movieId={movieId} isSelected={props.seats[val] !== 0 ? true : false}/>
          })
        }
      </div>
      <div className='flex'><button className="btn" onClick={onConfirmHandler}>Confirm</button></div>
    </>
  );
}

const mapStateToProps = state => {
  return {
    seats: state.seat.seats,
    user: state.login.user.email
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getSeats: (id) => dispatch(getSeatHandler(id)),
    selectSeat: (seats, movieId, user) => dispatch(selectSeatHandler(seats, movieId, user))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Seats));