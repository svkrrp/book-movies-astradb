import { useState } from 'react';
import { connect } from 'react-redux';
import './SignupCard.scss';
import { submitSignupHandler } from '../../store/Login/actions';

const SignupCard = (props) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassowrd, setConfirmPassowrd] = useState("");

  const onSubmitHandler = (e) => {
    e.preventDefault();
    if(password === confirmPassowrd) {
      props.submitHandler(email, password);
    } else {
      alert('passord and confirmPassowrd are not same');
    }
  }

  return (
    <div className="SignupCard">
      <div>Sign Up</div>
      <form onSubmit={onSubmitHandler}>
        <input type="text" placeholder="email" onChange={(e) => setEmail(e.target.value)} />
        <input type="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
        <input type="password" placeholder="confirm Passowrd" onChange={(e) => setConfirmPassowrd(e.target.value)} />
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    submitHandler: (email, password) => dispatch(submitSignupHandler(email, password))
  }
}

export default connect(null, mapDispatchToProps)(SignupCard);
