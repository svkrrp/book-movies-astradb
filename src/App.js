import { Switch, Route } from "react-router-dom";
import { connect } from 'react-redux';
import './App.css';
import SignUp from "./containers/Signup/Signup";
import Login from "./containers/Login/Login";
import Home from "./containers/Home/Home";
import MovieUpload from "./containers/MovieUpload/MovieUpload";
import Hall from "./containers/Hall/Hall";

function App(props) {
  console.log(props.user);
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={props.user ? Home : Login} />
        <Route exact path="/signup" component={props.user ? SignUp : Login} />
        <Route exact path="/movieupload" component={props.user ? MovieUpload : Login} />
        <Route exact path="/hall/:id" component={props.user ? Hall : Login} />
      </Switch>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    user: state.login.user
  }
}

export default connect(mapStateToProps)(App);
