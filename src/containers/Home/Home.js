import { connect } from 'react-redux';
import { useEffect } from 'react';

import MovieList from "../../components/MovieList/MovieList";
import Nav from '../../components/Nav/Nav';
import { getMovies } from '../../store/Movie/actions';

const Home = (props) => {

  useEffect(() => {
    props.getMovies()
  }, [])

  return (
    <div className="Home">
      <Nav />
      <MovieList movies={props.movies} />
    </div>
  );
}

const mapStateToProps = state => {
  return {
    movies: state.movie.movies
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getMovies: () => dispatch(getMovies())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
