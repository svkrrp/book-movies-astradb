import './Hall.scss';
import Nav from "../../components/Nav/Nav";
import Seats from "../../components/Seats/Seats";

const Hall = (props) => {
  return (
    <div>
      <Nav />
      <Seats />
    </div>
  );
}

export default Hall;