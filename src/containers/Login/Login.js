import Nav from "../../components/Nav/Nav";
import LoginCard from '../../components/LoginCard/LoginCard'

const Login = (props) => {
  return (
    <div>
      <Nav />
      <LoginCard />
    </div>
  );
}

export default Login;