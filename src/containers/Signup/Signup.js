import Nav from "../../components/Nav/Nav";
import SignupCard from '../../components/SignupCard/SignupCard'

const SignUp = (props) => {
  return (
    <div>
      <Nav />
      <SignupCard />
    </div>
  );
}

export default SignUp;