import { useState } from 'react';
import { connect } from 'react-redux';

import Nav from "../../components/Nav/Nav";
import './MovieUpload.scss';
import { addMovieHandler } from '../../store/Movie/actions';

const MovieUpload = (props) => {

  const [name, setName] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [time, setTime] = useState("");

  const onSubmitHandler = (e) => {
    e.preventDefault();
    if(name !== null && imageUrl !== null && time !== null) {
      props.addMovie({
        name,
        imageUrl,
        time
      });
    } else {
      alert('Fill details completely');
    }
  }

  return (
    <div>
      <Nav />
      <div className="MovieUpload">
        <div>Movie Upload</div>
        <form onSubmit={onSubmitHandler}>
          <input type="text" placeholder="Movie Name" onChange={(e) => setName(e.target.value)} />
          <input type="text" placeholder="Movie Image Url" onChange={(e) => setImageUrl(e.target.value)} />
          <input type="text" placeholder="Set Time" onChange={(e) => setTime(e.target.value)} />
          <input type="submit" value="Submit" />
        </form>
      </div>
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    addMovie: (movie) => dispatch(addMovieHandler(movie))
  }
}

export default connect(null, mapDispatchToProps)(MovieUpload);