import * as actionTypes from '../types';

const InitialState = {
  seats: []
}

const reducer = (state = InitialState, action) => {
  switch(action.type) {
    case actionTypes.SET_SEATS:
      return {
        ...state,
        seats: action.seats
      }
    default:
      return state;
  }
}

export default reducer;