import axios from 'axios';
import * as actionTypes from '../types';

const setSeats = (seats) => {
  return {
    type: actionTypes.SET_SEATS,
    seats
  }
}

export const selectSeatHandler = (seats, movieId, user) => {
  return dispatch => {
    axios.post(`https://stormy-fjord-02469.herokuapp.com/seats`, {
      movieId,
      seats,
      user
    }).then((res) => {
      console.log('success', res);
      dispatch(setSeats(res.data));
    }).catch((error) => {
      console.log(error);
    })
  }
}

export const getSeatHandler = (movieId) => {
  return dispatch => {
    console.log(movieId);
    axios.get(`https://stormy-fjord-02469.herokuapp.com/seats/${movieId}`).then((res) => {
      dispatch(setSeats(res.data))
    }).catch((error) => {
      console.log(error);
    })
  }
}