import * as actionTypes from '../types';

const InitialState = {
  user: null,
  token: null
}

const reducer = (state = InitialState, action) => {
  switch(action.type) {
    case actionTypes.SET_USER:
      return {
        ...state,
        user: action.user
      }
    default:
      return state;
  }
}

export default reducer;