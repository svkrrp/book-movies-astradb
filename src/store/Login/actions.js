import axios from 'axios';
import * as actionTypes from '../types';

const setUser = (user) => {
  return {
    type: actionTypes.SET_USER,
    user
  }
}

export const submitLoginHandler = (email, password) => {
  return dispatch => {
    axios.post('https://stormy-fjord-02469.herokuapp.com/login', {
      email,
      password
    }).then((res) => {
      console.log(res.data);
      dispatch(setUser(res.data))
    })
    .catch(error => {
      console.log(error);
    })
  }
}

export const submitSignupHandler = (email, password) => {
  return dispatch => {
    axios.post('https://stormy-fjord-02469.herokuapp.com/signup', {
      email,
      password
    }).then((res) => {
      console.log(res.data);
      dispatch(setUser(res.data))
    })
    .catch(error => {
      console.log(error);
    })
  }
}

export const logoutHandler = () => {
  return dispatch => {
    axios.post('https://stormy-fjord-02469.herokuapp.com/logout').then((res) => {
      dispatch(setUser(null));
    })
    .catch(error => {
      console.log(error);
    })
  }
}