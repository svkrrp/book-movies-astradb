import axios from 'axios';
import * as actionTypes from '../types';

const setMovies = (movies) => {
  return {
    type: actionTypes.SET_MOVIES,
    movies
  }
}

const removeMovie = (id) => {
  return {
    type: actionTypes.REMOVE_MOVIE,
    id
  }
}

export const addMovieHandler = (movie) => {
  return dispatch => {
    axios.post('https://stormy-fjord-02469.herokuapp.com/movie/addmovie', {
      movie
    }).then(res => {
      console.log("Movie Added ", res);
    }).catch((error) => {
      console.log(error);
    })
  }
}

export const getMovies = () => {
  return dispatch => {
    axios.get('https://stormy-fjord-02469.herokuapp.com/movie/movies')
      .then(res => {
        console.log(res.data);
        dispatch(setMovies(res.data));
      })
      .catch(error => {
        console.log(error);
      })
  }
}

export const removeMovieHandler = (id) => {
  return dispatch => {
    axios.post(`https://stormy-fjord-02469.herokuapp.com/movie/delete/${id}`)
      .then((res) => {
        console.log("Movie Deleted" + res);
        dispatch(removeMovie(id));
      })
      .catch(error => {
        console.log(error);
      })
  }
}