import * as actionTypes from '../types';

const InitialState = {
  movies: []
}

const reducer = (state = InitialState, action) => {
  switch(action.type) {
    case actionTypes.SET_MOVIES:
      return {
        ...state,
        movies: action.movies
      }
    case actionTypes.REMOVE_MOVIE:
      return {
        ...state,
        movies: state.movies.filter(movie => movie.id !== action.id)
      }
    default:
      return state;
  }
}

export default reducer;