export const SET_USER = 'SET_USER';
export const SET_MOVIES = 'SET_MOVIES';
export const REMOVE_MOVIE = 'REMOVE_MOVIE';
export const SET_SEATS = "SET_SEATS";
export const SELECTED_SEATS = 'SELECTED_SEATS';
