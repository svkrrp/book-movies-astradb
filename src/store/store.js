import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import login from './Login/reducer';
import movie from './Movie/reducer';
import seat from './Seat/reducer';

const rootReducer = combineReducers({
  login,
  movie,
  seat
})

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
